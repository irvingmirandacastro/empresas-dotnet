﻿using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Login;
using empresas_dotnet.Domain.Response.Login;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace empresas_dotnet.Application.Controllers
{
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService LoginService;
        private readonly IConfiguration Config;
        public LoginController(ILoginService LoginService, IConfiguration Config)
        {
            this.LoginService = LoginService;
            this.Config = Config;
        }
        [HttpPost("/admin/login")]
        public async Task<LoginResponse> LoginAdmin([FromBody] LoginParam loginRequest)
        {
            return await LoginService.LoginAdmin(loginRequest, Config.GetValue<string>("App:TokenSecret"));
        }
        [HttpPost]
        public async Task<LoginResponse> LoginComum([FromBody] LoginParam loginRequest)
        {
            return await LoginService.LoginComum(loginRequest, Config.GetValue<string>("App:TokenSecret"));
        }
    }
}

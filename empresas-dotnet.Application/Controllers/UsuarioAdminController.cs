﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Usuario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.Application.Controllers
{
    [Route("admin/usuarios")]
    [ApiController]
    public class UsuarioAdminController : ControllerBase
    {
        private readonly IUsuarioAdminService Service;
        public UsuarioAdminController(IUsuarioAdminService Service)
        {
            this.Service = Service;
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<dynamic>> PostUsuarioAdmin([FromBody] UsuarioPostParam newUsuario)
        {
            await Service.NovoUsuario(newUsuario);
            return Ok();
        }
        [HttpPut]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<dynamic>> PutUsuarioAdmin([FromBody] UsuarioPutParam newUsuario)
        {
            Guid UsuarioAdminId = Guid.Parse(User.Claims.Where(c => c.Type == "Id").Single().Value);
            await Service.EditaUsuario(newUsuario, UsuarioAdminId);
            return Ok();
        }
        [HttpDelete("{UsuarioAdminId:Guid}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<dynamic>> SoftDeleteUsuarioAdmin(Guid UsuarioAdminId)
        {
            await Service.DeleteUsuario(UsuarioAdminId);
            return Ok();
        }
    }
}

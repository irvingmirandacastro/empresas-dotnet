﻿using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Filme;
using empresas_dotnet.Domain.Requests.Voto;
using empresas_dotnet.Domain.Response.Filme;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace empresas_dotnet.Application.Controllers
{
    [Route("api/filmes")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly IFilmeService Service;
        public FilmeController(IFilmeService Service)
        {
            this.Service = Service;
        }
        [HttpGet("{FilmeId:Guid}")]
        public async Task<DetalhesFilmeDTO> GetFilme(Guid FilmeId)
        {
            return await Service.GetFilme(FilmeId);
        }
        [HttpGet]
        public async Task<ICollection<ListagemFilmesDTO>> GetFilmes(string Diretor, string Nome, Guid? CategoriaId, string Ator, int? Pag, int FilmesPorPag = 2)
        {
            return await Service.GetFilmes(Diretor, Nome, CategoriaId, Ator, Pag, FilmesPorPag);
        }
        [HttpPost("/admin/filmes")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<dynamic>> PostFilme([FromBody] FilmePostParam newFilme)
        {
            await Service.CadastrarFilme(newFilme);
            return Ok();
        }
        [HttpPost("/votar")]
        [Authorize(Roles = "Usuario")]
        public async Task<ActionResult<dynamic>> PostVoto([FromBody] VotoPostParam newVoto)
        {
            Guid UsuarioComumId = Guid.Parse(User.Claims.Where(c => c.Type == "Id").Single().Value);
            await Service.Votar(UsuarioComumId, newVoto);
            return Ok();
        }
    }
}

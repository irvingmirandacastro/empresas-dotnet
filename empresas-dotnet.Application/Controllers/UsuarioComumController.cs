﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Usuario;
using empresas_dotnet.Domain.Response.Usuario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace empresas_dotnet.Application.Controllers
{
    [Route("api/usuarios")]
    [ApiController]
    public class UsuarioComumController : ControllerBase
    {
        private readonly IUsuarioComumService Service;
        public UsuarioComumController(IUsuarioComumService Service)
        {
            this.Service = Service;
        }
        [HttpPost]
        public async Task<ActionResult<dynamic>> PostUsuarioComum([FromBody] UsuarioPostParam newUsuario)
        {
            await Service.NovoUsuario(newUsuario);
            return Ok();
        }
        [HttpPut]
        [Authorize(Roles = "Usuario")]
        public async Task<ActionResult<dynamic>> PutUsuarioComum([FromBody] UsuarioPutParam newUsuario)
        {
            Guid UsuarioComumId = Guid.Parse(User.Claims.Where(c => c.Type == "Id").Single().Value);
            await Service.EditaUsuario(newUsuario, UsuarioComumId);
            return Ok();
        }
        [HttpDelete("{UsuarioComumId:Guid}")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<dynamic>> SoftDeleteUsuarioComum(Guid UsuarioComumId)
        {
            await Service.DeleteUsuario(UsuarioComumId);
            return Ok();
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ICollection<ListagemUsuarioDTO>> SoftDeleteUsuarioAdmin(int? Pag, int UsuariosPorPag = 2)
        {
            return await Service.GetUsuarios(Pag, UsuariosPorPag);
        }
    }
}

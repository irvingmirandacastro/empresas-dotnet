﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;

namespace empresas_dotnet.Application.Config
{
    public static class AutoMapperConfig
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            var assemblies = new List<Assembly> { Assembly.Load("empresas-dotnet.Domain") };
            services.AddAutoMapper(assemblies);
        }
    }
}

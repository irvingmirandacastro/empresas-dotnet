﻿using empresas_dotnet.Data.Repositories;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Service.Services;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Application.Config
{
    public static class ServiceConfig
    {
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IUsuarioAdminService, UsuarioAdminService>();
            services.AddScoped<IUsuarioComumService, UsuarioComumService>();
        }
    }
}

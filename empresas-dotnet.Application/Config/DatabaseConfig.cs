﻿using empresas_dotnet.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace empresas_dotnet.Application.Config
{
    public static class DatabaseConfig
    {
        public static void ConfigureDatabase(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<IMDBContext>(opts =>
                    opts.UseSqlServer(config["App:ConnectionString"]));
        }
    }
}

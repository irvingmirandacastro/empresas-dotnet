﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace empresas_dotnet.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Atores",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Atores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Filmes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Diretor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Descricao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PosterURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filmes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsuariosAdmins",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Senha = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariosAdmins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsuariosComuns",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Senha = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariosComuns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AtoresFilmes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    AtorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FilmeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AtoresFilmes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AtoresFilmes_Atores_AtorId",
                        column: x => x.AtorId,
                        principalTable: "Atores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AtoresFilmes_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CategoriasFilmes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CategoriaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FilmeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoriasFilmes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CategoriasFilmes_Categorias_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoriasFilmes_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Votos",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UsuarioComumId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FilmeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nota = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Votos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Votos_Filmes_FilmeId",
                        column: x => x.FilmeId,
                        principalTable: "Filmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Votos_UsuariosComuns_UsuarioComumId",
                        column: x => x.UsuarioComumId,
                        principalTable: "UsuariosComuns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Atores",
                columns: new[] { "Id", "Ativo", "CreatedAt", "Nome", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("ee18fcce-80ac-4a06-aa8d-66c7568f8890"), true, new DateTime(2021, 3, 18, 1, 35, 11, 360, DateTimeKind.Local).AddTicks(4231), "Jhon Doe", null },
                    { new Guid("e3c43330-2f24-4d36-bba1-24488074295b"), true, new DateTime(2021, 3, 18, 1, 35, 11, 361, DateTimeKind.Local).AddTicks(2728), "Juliana", null }
                });

            migrationBuilder.InsertData(
                table: "Categorias",
                columns: new[] { "Id", "Ativo", "CreatedAt", "Nome", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("53887fce-6cfe-415e-a8d2-8500c54b2d30"), true, new DateTime(2021, 3, 18, 1, 35, 11, 362, DateTimeKind.Local).AddTicks(5064), "Ação", null },
                    { new Guid("afacddec-0733-4872-9512-a2e26ace585d"), true, new DateTime(2021, 3, 18, 1, 35, 11, 362, DateTimeKind.Local).AddTicks(5584), "Drama", null }
                });

            migrationBuilder.InsertData(
                table: "Filmes",
                columns: new[] { "Id", "Ativo", "CreatedAt", "Descricao", "Diretor", "Nome", "PosterURL", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("7014bd72-44bf-4605-beb3-1020bed14268"), true, new DateTime(2021, 3, 18, 1, 35, 11, 362, DateTimeKind.Local).AddTicks(6669), "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.", "Frank Darabont", "The Shawshank Redemption", "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg", null },
                    { new Guid("7ca15c34-f1c8-4922-8feb-0aaed6856d26"), true, new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(667), "An organized crime dynasty's aging patriarch transfers control of his clandestine empire to his reluctant son.", "Francis Ford Coppola", "The Godfather", "https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg", null },
                    { new Guid("134dbd34-5304-43d8-ad9e-6ccb23101dbb"), true, new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(855), "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.", "Christopher Nolan", "The Dark Knight", "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg", null }
                });

            migrationBuilder.InsertData(
                table: "UsuariosAdmins",
                columns: new[] { "Id", "Ativo", "CreatedAt", "Email", "Nome", "Senha", "UpdatedAt" },
                values: new object[] { new Guid("d81f1d1e-9bed-4115-9f58-806b88a32f82"), true, new DateTime(2021, 3, 18, 1, 35, 11, 366, DateTimeKind.Local).AddTicks(9525), "jhon@teste.com", "Jhon Doe", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f", null });

            migrationBuilder.InsertData(
                table: "UsuariosComuns",
                columns: new[] { "Id", "Ativo", "CreatedAt", "Email", "Nome", "Senha", "UpdatedAt" },
                values: new object[] { new Guid("eaef57ad-f146-4ac5-9bbb-b3c5b6e77c8b"), true, new DateTime(2021, 3, 18, 1, 35, 11, 366, DateTimeKind.Local).AddTicks(7658), "joao@teste.com", "João do Caminhão", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f", null });

            migrationBuilder.InsertData(
                table: "AtoresFilmes",
                columns: new[] { "Id", "Ativo", "AtorId", "CreatedAt", "FilmeId", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("ca223589-d3d6-4b0f-86f7-9b596fddc5e9"), true, new Guid("ee18fcce-80ac-4a06-aa8d-66c7568f8890"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(2110), new Guid("7014bd72-44bf-4605-beb3-1020bed14268"), null },
                    { new Guid("247547fb-c4d1-4efd-9230-0bfc48c89b61"), true, new Guid("ee18fcce-80ac-4a06-aa8d-66c7568f8890"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(3575), new Guid("7ca15c34-f1c8-4922-8feb-0aaed6856d26"), null },
                    { new Guid("c6ddb65b-292b-47d1-9a9f-9b60a6db580b"), true, new Guid("e3c43330-2f24-4d36-bba1-24488074295b"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(3713), new Guid("7ca15c34-f1c8-4922-8feb-0aaed6856d26"), null },
                    { new Guid("ee9fddb1-7848-461e-a603-a05a93957641"), true, new Guid("e3c43330-2f24-4d36-bba1-24488074295b"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(3550), new Guid("134dbd34-5304-43d8-ad9e-6ccb23101dbb"), null }
                });

            migrationBuilder.InsertData(
                table: "CategoriasFilmes",
                columns: new[] { "Id", "Ativo", "CategoriaId", "CreatedAt", "FilmeId", "UpdatedAt" },
                values: new object[,]
                {
                    { new Guid("f6e983f8-8146-4a94-9d2e-60a9005e819d"), true, new Guid("afacddec-0733-4872-9512-a2e26ace585d"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(5987), new Guid("7014bd72-44bf-4605-beb3-1020bed14268"), null },
                    { new Guid("70d4adfa-4c08-4677-bf20-1b90d97ab239"), true, new Guid("53887fce-6cfe-415e-a8d2-8500c54b2d30"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(6011), new Guid("7ca15c34-f1c8-4922-8feb-0aaed6856d26"), null },
                    { new Guid("83ea9d3c-742f-47a2-a753-43aa769c849e"), true, new Guid("afacddec-0733-4872-9512-a2e26ace585d"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(6015), new Guid("7ca15c34-f1c8-4922-8feb-0aaed6856d26"), null },
                    { new Guid("b4cb7fb2-9090-41c4-958b-9331afd8a6fa"), true, new Guid("53887fce-6cfe-415e-a8d2-8500c54b2d30"), new DateTime(2021, 3, 18, 1, 35, 11, 363, DateTimeKind.Local).AddTicks(4826), new Guid("134dbd34-5304-43d8-ad9e-6ccb23101dbb"), null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AtoresFilmes_AtorId",
                table: "AtoresFilmes",
                column: "AtorId");

            migrationBuilder.CreateIndex(
                name: "IX_AtoresFilmes_FilmeId",
                table: "AtoresFilmes",
                column: "FilmeId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoriasFilmes_CategoriaId",
                table: "CategoriasFilmes",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoriasFilmes_FilmeId",
                table: "CategoriasFilmes",
                column: "FilmeId");

            migrationBuilder.CreateIndex(
                name: "IX_UsuariosAdmins_Email",
                table: "UsuariosAdmins",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UsuariosComuns_Email",
                table: "UsuariosComuns",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Votos_FilmeId",
                table: "Votos",
                column: "FilmeId");

            migrationBuilder.CreateIndex(
                name: "IX_Votos_UsuarioComumId",
                table: "Votos",
                column: "UsuarioComumId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AtoresFilmes");

            migrationBuilder.DropTable(
                name: "CategoriasFilmes");

            migrationBuilder.DropTable(
                name: "UsuariosAdmins");

            migrationBuilder.DropTable(
                name: "Votos");

            migrationBuilder.DropTable(
                name: "Atores");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "Filmes");

            migrationBuilder.DropTable(
                name: "UsuariosComuns");
        }
    }
}

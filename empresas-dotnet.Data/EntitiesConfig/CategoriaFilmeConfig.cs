﻿using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Data.EntitiesConfig
{
    public class CategoriaFilmeConfig : IEntityTypeConfiguration<CategoriaFilme>
    {
        public void Configure(EntityTypeBuilder<CategoriaFilme> builder)
        {
            builder.ToTable("CategoriasFilmes");
            builder.HasOne(c => c.Filme).WithMany(c => c.Categorias).HasForeignKey(c => c.FilmeId);
            builder.HasOne(c => c.Categoria).WithMany(c => c.Filmes).HasForeignKey(c => c.CategoriaId);
        }
    }
}

﻿using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Data.EntitiesConfig
{
    public class UsuarioAdminConfig : IEntityTypeConfiguration<UsuarioAdmin>
    {
        public void Configure(EntityTypeBuilder<UsuarioAdmin> builder)
        {
            builder.ToTable("UsuariosAdmins");

            builder.HasIndex(c => c.Email).IsUnique();

            builder.Property(c => c.Email).IsRequired(true);
        }
    }
}

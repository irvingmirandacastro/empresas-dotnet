﻿using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Data.EntitiesConfig
{
    public class AtorFilmeConfig : IEntityTypeConfiguration<AtorFilme>
    {
        public void Configure(EntityTypeBuilder<AtorFilme> builder)
        {
            builder.ToTable("AtoresFilmes");
            builder.HasOne(c => c.Filme).WithMany(c => c.Atores).HasForeignKey(c => c.FilmeId);
            builder.HasOne(c => c.Ator).WithMany(c => c.Filmes).HasForeignKey(c => c.AtorId);
        }
    }
}

﻿using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Data.EntitiesConfig
{
    public class VotoConfig : IEntityTypeConfiguration<Voto>
    {
        public void Configure(EntityTypeBuilder<Voto> builder)
        {
            builder.ToTable("Votos");
            builder.HasOne(c => c.Filme).WithMany(c => c.Votos).HasForeignKey(c => c.FilmeId);
            builder.HasOne(c => c.UsuarioComum).WithMany(c => c.Votos).HasForeignKey(c => c.UsuarioComumId);
        }
    }
}

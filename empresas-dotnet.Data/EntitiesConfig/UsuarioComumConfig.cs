﻿using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace empresas_dotnet.Data.EntitiesConfig
{
    public class UsuarioComumConfig : IEntityTypeConfiguration<UsuarioComum>
    {
        public void Configure(EntityTypeBuilder<UsuarioComum> builder)
        {
            builder.ToTable("UsuariosComuns");

            builder.HasIndex(c => c.Email).IsUnique();

            builder.Property(c => c.Email).IsRequired(true);

            builder.HasMany(c => c.Votos).WithOne(c => c.UsuarioComum).HasForeignKey(c => c.UsuarioComumId);
        }
    }
}

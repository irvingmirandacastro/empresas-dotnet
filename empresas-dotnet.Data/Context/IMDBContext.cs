﻿using empresas_dotnet.CrossCutting;
using empresas_dotnet.Data.EntitiesConfig;
using empresas_dotnet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace empresas_dotnet.Data.Context
{
    public class IMDBContext : DbContext
    {
        public IMDBContext(DbContextOptions<IMDBContext> options) : base(options) { }
        public IMDBContext() { }
        public DbSet<UsuarioComum> UsuariosComuns { get; set; }
        public DbSet<UsuarioAdmin> UsuariosAdmin { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<Voto> Votos { get; set; }
        public DbSet<Ator> Atores { get; set; }
        public DbSet<AtorFilme> AtoresFilmes { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<CategoriaFilme> CategoriasFilmes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Filme>(new FilmeConfig().Configure);
            builder.Entity<UsuarioAdmin>(new UsuarioAdminConfig().Configure);
            builder.Entity<UsuarioComum>(new UsuarioComumConfig().Configure);
            builder.Entity<Voto>(new VotoConfig().Configure);
            builder.Entity<Ator>(new AtorConfig().Configure);
            builder.Entity<AtorFilme>(new AtorFilmeConfig().Configure);
            builder.Entity<Categoria>(new CategoriaConfig().Configure);
            builder.Entity<CategoriaFilme>(new CategoriaFilmeConfig().Configure);
            Seed(builder);
        }
        private void Seed(ModelBuilder builder)
        {
            builder.Entity<Ator>().HasData(
                new Ator("Jhon Doe") { Id = Guid.Parse("EE18FCCE-80AC-4A06-AA8D-66C7568F8890") },
                new Ator("Juliana") { Id = Guid.Parse("E3C43330-2F24-4D36-BBA1-24488074295B") }
            );
            builder.Entity<Categoria>().HasData(
                new Categoria("Ação") { Id = Guid.Parse("53887FCE-6CFE-415E-A8D2-8500C54B2D30") },
                new Categoria("Drama") { Id = Guid.Parse("AFACDDEC-0733-4872-9512-A2E26ACE585D") }
            );

            builder.Entity<Filme>().HasData(
                new Filme("The Shawshank Redemption", "Frank Darabont",
                "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
                "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UX182_CR0,0,182,268_AL_.jpg") { Id = Guid.Parse("7014bd72-44bf-4605-beb3-1020bed14268") },
                new Filme("The Godfather", "Francis Ford Coppola",
                "An organized crime dynasty's aging patriarch transfers control of his clandestine empire to his reluctant son.",
                "https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg") { Id = Guid.Parse("7ca15c34-f1c8-4922-8feb-0aaed6856d26") },
                new Filme("The Dark Knight", "Christopher Nolan",
                "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.",
                "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg") { Id = Guid.Parse("134dbd34-5304-43d8-ad9e-6ccb23101dbb") }
            );
            builder.Entity<AtorFilme>().HasData(
                new AtorFilme(Guid.Parse("EE18FCCE-80AC-4A06-AA8D-66C7568F8890"), Guid.Parse("7014bd72-44bf-4605-beb3-1020bed14268")),
                new AtorFilme(Guid.Parse("E3C43330-2F24-4D36-BBA1-24488074295B"), Guid.Parse("134dbd34-5304-43d8-ad9e-6ccb23101dbb")),
                new AtorFilme(Guid.Parse("EE18FCCE-80AC-4A06-AA8D-66C7568F8890"), Guid.Parse("7ca15c34-f1c8-4922-8feb-0aaed6856d26")),
                new AtorFilme(Guid.Parse("E3C43330-2F24-4D36-BBA1-24488074295B"), Guid.Parse("7ca15c34-f1c8-4922-8feb-0aaed6856d26"))
            );
            builder.Entity<CategoriaFilme>().HasData(
                new CategoriaFilme(Guid.Parse("53887FCE-6CFE-415E-A8D2-8500C54B2D30"), Guid.Parse("134dbd34-5304-43d8-ad9e-6ccb23101dbb")),
                new CategoriaFilme(Guid.Parse("AFACDDEC-0733-4872-9512-A2E26ACE585D"), Guid.Parse("7014bd72-44bf-4605-beb3-1020bed14268")),
                new CategoriaFilme(Guid.Parse("53887FCE-6CFE-415E-A8D2-8500C54B2D30"), Guid.Parse("7ca15c34-f1c8-4922-8feb-0aaed6856d26")),
                new CategoriaFilme(Guid.Parse("AFACDDEC-0733-4872-9512-A2E26ACE585D"), Guid.Parse("7ca15c34-f1c8-4922-8feb-0aaed6856d26"))
            );
            builder.Entity<UsuarioComum>().HasData(
                new UsuarioComum("João do Caminhão", "joao@teste.com", Seguranca.cryptSHA256("12345678")) // Pendente Crypto
            );
            builder.Entity<UsuarioAdmin>().HasData(
                new UsuarioComum("Jhon Doe", "jhon@teste.com", Seguranca.cryptSHA256("12345678")) // Pendente Crypto
            );
        }
    }
}

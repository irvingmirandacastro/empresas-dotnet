﻿using empresas_dotnet.Data.Context;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace empresas_dotnet.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        IMDBContext context;
        public BaseRepository(IMDBContext context)
        {
            this.context = context;
        }
        public async virtual Task Create(T entity)
        {
            await context.Set<T>().AddAsync(entity);
            await context.SaveChangesAsync();
        }
        public async virtual Task CreateMany(ICollection<T> entities)
        {
            await context.Set<T>().AddRangeAsync(entities);
            await context.SaveChangesAsync();
        }

        public async virtual Task<ICollection<T>> Find(Expression<Func<T, bool>> expression, int? skip = null, int? take = null, ICollection<string> includes = null)
        {
            var query = context.Set<T>().Where(expression);

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return await LoadDependencies(query, includes).ToListAsync();
        }

        public async virtual Task<T> FindSingle(Expression<Func<T, bool>> expression, ICollection<string> includes = null)
        {
            var query = context.Set<T>().Where(expression);
            return await LoadDependencies(query, includes).FirstOrDefaultAsync();
        }

        private IQueryable<T> LoadDependencies(IQueryable<T> query, IEnumerable<string> includes)
        {
            if (includes != null)
            {
                foreach (var include in includes)
                {
                    query = query.Include(include);
                }
            }
            return query;
        }
        public async virtual Task Save()
        {
            await context.SaveChangesAsync();
        }
    }
}

﻿using empresas_dotnet.Data.Context;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;

namespace empresas_dotnet.Data.Repositories
{
    public class UsuarioAdminRepository : BaseRepository<UsuarioAdmin>, IUsuarioAdminRepository
    {
        public UsuarioAdminRepository(IMDBContext context) : base(context) { }
    }
}

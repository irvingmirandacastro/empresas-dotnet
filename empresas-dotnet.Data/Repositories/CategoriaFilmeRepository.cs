﻿using empresas_dotnet.Data.Context;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;

namespace empresas_dotnet.Data.Repositories
{
    public class CategoriaFilmeRepository : BaseRepository<CategoriaFilme>, ICategoriaFilmeRepository
    {
        public CategoriaFilmeRepository(IMDBContext context) : base(context) { }
    }
}

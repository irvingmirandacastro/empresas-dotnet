﻿using empresas_dotnet.Data.Context;
using empresas_dotnet.Domain.Interfaces.Repositories;

namespace empresas_dotnet.Data.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        public RepositoryWrapper(IMDBContext Context)
        {
            this.Context = Context;
        }
        private readonly IMDBContext Context;
        private IFilmeRepository FilmeRepository;
        private ICategoriaFilmeRepository CategoriaFilmeRepository;
        private IAtorFilmeRepository AtorFilmeRepository;
        private IVotoRepository VotoRepository;
        private IUsuarioComumRepository UsuarioComumRepository;
        private IUsuarioAdminRepository UsuarioAdminRepository;

        public IFilmeRepository Filmes
        { 
            get
            {
                if (FilmeRepository == null) FilmeRepository = new FilmeRepository(Context);
                return FilmeRepository;
            }
        }

        public ICategoriaFilmeRepository CategoriasFilmes
        {
            get
            {
                if (CategoriaFilmeRepository == null) CategoriaFilmeRepository = new CategoriaFilmeRepository(Context);
                return CategoriaFilmeRepository;
            }
        }

        public IAtorFilmeRepository AtoresFilmes
        {
            get
            {
                if (AtorFilmeRepository == null) AtorFilmeRepository = new AtorFilmeRepository(Context);
                return AtorFilmeRepository;
            }
        }
        public IVotoRepository Votos
        {
            get
            {
                if (VotoRepository == null) VotoRepository = new VotoRepository(Context);
                return VotoRepository;
            }
        }

        public IUsuarioAdminRepository UsuariosAdmins
        {
            get
            {
                if (UsuarioAdminRepository == null) UsuarioAdminRepository = new UsuarioAdminRepository(Context);
                return UsuarioAdminRepository;
            }
        }

        public IUsuarioComumRepository UsuariosComuns
        {
            get
            {
                if (UsuarioComumRepository == null) UsuarioComumRepository = new UsuarioComumRepository(Context);
                return UsuarioComumRepository;
            }
        }
    }
}

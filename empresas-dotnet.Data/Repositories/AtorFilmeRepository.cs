﻿using empresas_dotnet.Data.Context;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;

namespace empresas_dotnet.Data.Repositories
{
    public class AtorFilmeRepository : BaseRepository<AtorFilme>, IAtorFilmeRepository
    {
        public AtorFilmeRepository(IMDBContext context) : base(context) { }
    }
}

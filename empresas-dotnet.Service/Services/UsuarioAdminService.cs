﻿using AutoMapper;
using empresas_dotnet.CrossCutting;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Usuario;
using System;
using System.Threading.Tasks;

namespace empresas_dotnet.Service.Services
{
    public class UsuarioAdminService : IUsuarioAdminService
    {
        private readonly IRepositoryWrapper Repository;
        private readonly IMapper Mapper;
        public UsuarioAdminService(IRepositoryWrapper Repository, IMapper Mapper)
        {
            this.Repository = Repository;
            this.Mapper = Mapper;
        }
        public async Task DeleteUsuario(Guid UsuarioAdminId)
        {
            UsuarioAdmin usuario = await Repository.UsuariosAdmins.FindSingle(c => c.Ativo && c.Id == UsuarioAdminId);
            usuario.TrocaEstado(false);
            await Repository.UsuariosAdmins.Save();
        }

        public async Task EditaUsuario(UsuarioPutParam newUsuario, Guid UsuarioAdminId)
        {
            UsuarioAdmin usuario = await Repository.UsuariosAdmins.FindSingle(c => c.Ativo && c.Id == UsuarioAdminId);
            usuario.Update(newUsuario.Nome, newUsuario.Email, newUsuario.Senha != null ?Seguranca.cryptSHA256(newUsuario.Senha) : null);
            await Repository.UsuariosAdmins.Save();
        }

        public async Task NovoUsuario(UsuarioPostParam newUsuario)
        {
            UsuarioAdmin usuario = new UsuarioAdmin(newUsuario.Nome, newUsuario.Email, Seguranca.cryptSHA256(newUsuario.Senha));
            await Repository.UsuariosAdmins.Create(usuario);
        }
    }
}

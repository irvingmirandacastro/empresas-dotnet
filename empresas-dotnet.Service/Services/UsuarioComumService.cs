﻿using AutoMapper;
using empresas_dotnet.CrossCutting;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Usuario;
using empresas_dotnet.Domain.Response.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotnet.Service.Services
{
    public class UsuarioComumService : IUsuarioComumService
    {
        private readonly IRepositoryWrapper Repository;
        private readonly IMapper Mapper;
        public UsuarioComumService(IRepositoryWrapper Repository, IMapper Mapper)
        {
            this.Repository = Repository;
            this.Mapper = Mapper;
        }
        public async Task DeleteUsuario(Guid UsuarioComumId)
        {
            UsuarioComum usuario = await Repository.UsuariosComuns.FindSingle(c => c.Ativo && c.Id == UsuarioComumId);
            usuario.TrocaEstado(false);
            await Repository.UsuariosComuns.Save();
        }

        public async Task EditaUsuario(UsuarioPutParam newUsuario, Guid UsuarioComumId)
        {
            UsuarioComum usuario = await Repository.UsuariosComuns.FindSingle(c => c.Ativo && c.Id == UsuarioComumId);
            usuario.Update(newUsuario.Nome, newUsuario.Email, newUsuario.Senha != null ? Seguranca.cryptSHA256(newUsuario.Senha) : null);
            await Repository.UsuariosComuns.Save();
        }

        public async Task<ICollection<ListagemUsuarioDTO>> GetUsuarios(int? Pag, int UsuariosPorPag)
        {
            ICollection<UsuarioComum> usuarios = await Repository.UsuariosComuns.Find(c => c.Ativo, 
                take: (!Pag.HasValue ? null : Pag * UsuariosPorPag),
                skip: (!Pag.HasValue ? null : (Pag - 1) * UsuariosPorPag));
            return Mapper.Map<ICollection<ListagemUsuarioDTO>>(usuarios).OrderBy(c => c.Nome).ToList();
        }

        public async Task NovoUsuario(UsuarioPostParam newUsuario)
        {
            UsuarioComum usuario = new UsuarioComum(newUsuario.Nome, newUsuario.Email, Seguranca.cryptSHA256(newUsuario.Senha));
            await Repository.UsuariosComuns.Create(usuario);
        }
    }
}

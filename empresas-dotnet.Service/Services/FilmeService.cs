﻿using AutoMapper;
using empresas_dotnet.CrossCutting;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Filme;
using empresas_dotnet.Domain.Requests.Voto;
using empresas_dotnet.Domain.Response.Filme;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace empresas_dotnet.Service.Services
{
    public class FilmeService : IFilmeService
    {
        private readonly IRepositoryWrapper Repository;
        private readonly IMapper Mapper;
        public FilmeService(IRepositoryWrapper Repository, IMapper Mapper)
        {
            this.Repository = Repository;
            this.Mapper = Mapper;
        }
        public async Task CadastrarFilme(FilmePostParam newFilme)
        {
            Filme filme = new Filme(newFilme.Nome, newFilme.Diretor, newFilme.Descricao, newFilme.PosterURL);
            List<CategoriaFilme> categoriasfilmes = new List<CategoriaFilme>();
            foreach (var CategoriaId in newFilme.CateogoriasIds)
            {
                categoriasfilmes.Add(new CategoriaFilme(CategoriaId, filme.Id));
            }
            List<AtorFilme> atoresfilmes = new List<AtorFilme>();
            foreach (var AtorId in newFilme.AtoresIds)
            {
                atoresfilmes.Add(new AtorFilme(AtorId, filme.Id));
            }
            await Repository.Filmes.Create(filme);
            await Repository.AtoresFilmes.CreateMany(atoresfilmes);
            await Repository.CategoriasFilmes.CreateMany(categoriasfilmes);
        }

        public async Task<DetalhesFilmeDTO> GetFilme(Guid FilmeId)
        {
            Filme filme = await Repository.Filmes.FindSingle(c => c.Id == FilmeId, includes: new List<string> { "Categorias", "Atores", "Categorias.Categoria", "Atores.Ator", "Votos" });
            DetalhesFilmeDTO detalheFilme = Mapper.Map<DetalhesFilmeDTO>(filme);
            return detalheFilme;
        }

        public async Task<ICollection<ListagemFilmesDTO>> GetFilmes(string Diretor, string Nome, Guid? CategoriaId, string Ator, int? Pag, int FilmesPorPag)
        {
            ICollection<Filme> filmes = await Repository.Filmes.Find(c => c.Ativo && (Diretor == null ? true : c.Diretor == Diretor) 
                && (Nome == null ? true : c.Nome == Nome) 
                && (CategoriaId == null ? true : c.Categorias.Any(cat => cat.CategoriaId == CategoriaId))
                && (Ator == null ? true : c.Atores.Any(a => a.Ator.Nome == Ator)), includes: new List<string> { "Votos" }, 
                take: (!Pag.HasValue ? null : Pag * FilmesPorPag),
                skip: (!Pag.HasValue ? null : (Pag - 1) * FilmesPorPag)
            );
            ICollection<ListagemFilmesDTO> listagemFilmes = Mapper.Map<ICollection<ListagemFilmesDTO>>(filmes);
            return listagemFilmes.OrderByDescending(c => c.VotoCount).ThenBy(c => c.Nome).ToList();
        }

        public async Task Votar(Guid UsuarioComumId, VotoPostParam newVoto)
        {
            Voto voto = await Repository.Votos.FindSingle(c => c.FilmeId == newVoto.FilmeId && c.UsuarioComumId == UsuarioComumId);
            if (voto != null) throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Usuário já votou nesse filme");
            if (newVoto.Nota < 0 || newVoto.Nota > 4) throw new StatusCodeException(System.Net.HttpStatusCode.BadRequest, "Nota deve ser entre 0 e 4");
            voto = new Voto(UsuarioComumId, newVoto.FilmeId, newVoto.Nota);
            await Repository.Votos.Create(voto);
        }
    }
}

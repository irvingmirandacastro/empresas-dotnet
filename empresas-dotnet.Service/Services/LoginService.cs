﻿
using empresas_dotnet.CrossCutting;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Interfaces.Services;
using empresas_dotnet.Domain.Requests.Login;
using empresas_dotnet.Domain.Response.Login;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace empresas_dotnet.Service.Services
{
    public class LoginService : ILoginService
    {
        private readonly IRepositoryWrapper Repository;
        private readonly IConfiguration Configuration;
        public LoginService(IRepositoryWrapper Repository, IConfiguration Configuration)
        {
            this.Repository = Repository;
            this.Configuration = Configuration;
        }
        public async Task<LoginResponse> LoginAdmin(LoginParam login, string tokenKey)
        {
            var senhacriptografada = Seguranca.cryptSHA256(login.Senha);
            var usuario = await Repository.UsuariosAdmins.FindSingle(c => c.Ativo && c.Email == login.Email && c.Senha == senhacriptografada);
            if (usuario == null) throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "Email ou senha incorretos");
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(tokenKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Nome),
                    new Claim("Id",  usuario.Id.ToString()),
                    new Claim(ClaimTypes.Role, "Admin"),
                }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            var response = new LoginResponse()
            { 
                Token = token,
                TempoSessao = tokenDescriptor.Expires
            };
            return response;
        }

        public async Task<LoginResponse> LoginComum(LoginParam login, string tokenKey)
        {
            var senhacriptografada = Seguranca.cryptSHA256(login.Senha);
            var usuario = await Repository.UsuariosComuns.FindSingle(c => c.Ativo && c.Email == login.Email && c.Senha == senhacriptografada);
            if (usuario == null) throw new StatusCodeException(System.Net.HttpStatusCode.NotFound, "Email ou senha incorretos");
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(tokenKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Nome),
                    new Claim("Id",  usuario.Id.ToString()),
                    new Claim(ClaimTypes.Role, "Usuario"),
                }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            var response = new LoginResponse()
            {
                Token = token,
                TempoSessao = tokenDescriptor.Expires
            };
            return response;
        }
    }
}

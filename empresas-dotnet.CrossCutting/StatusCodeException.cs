﻿using System;
using System.Net;

namespace empresas_dotnet.CrossCutting
{
    public class StatusCodeException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public StatusCodeException(HttpStatusCode statusCode)
        {
            this.StatusCode = statusCode;
        }

        public StatusCodeException(HttpStatusCode statusCode, string message)
            : base(message)
        {
            this.StatusCode = statusCode;
        }

        public StatusCodeException(HttpStatusCode statusCode, Exception inner)
            : this(statusCode, inner.ToString()) { }
    }
}

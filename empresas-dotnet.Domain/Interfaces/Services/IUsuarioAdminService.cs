﻿using empresas_dotnet.Domain.Requests.Usuario;
using System;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Interfaces.Services
{
    public interface IUsuarioAdminService
    {
        Task NovoUsuario(UsuarioPostParam newUsuario);
        Task EditaUsuario(UsuarioPutParam newUsuario, Guid UsuarioAdminId);
        Task DeleteUsuario(Guid UsuarioAdminId);
    }
}

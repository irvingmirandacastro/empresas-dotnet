﻿using empresas_dotnet.Domain.Requests.Filme;
using empresas_dotnet.Domain.Requests.Voto;
using empresas_dotnet.Domain.Response.Filme;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Interfaces.Services
{
    public interface IFilmeService
    {
        Task<DetalhesFilmeDTO> GetFilme(Guid FilmeId);
        Task<ICollection<ListagemFilmesDTO>> GetFilmes(string Diretor, string Nome, Guid? CategoriaId, string Ator, int? Pag, int FilmesPorPag);
        Task CadastrarFilme(FilmePostParam newFilme);
        Task Votar(Guid UsuarioComumId, VotoPostParam newVoto);
    }
}

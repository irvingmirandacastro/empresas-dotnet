﻿using empresas_dotnet.Domain.Requests.Usuario;
using empresas_dotnet.Domain.Response.Usuario;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Interfaces.Services
{
    public interface IUsuarioComumService
    {
        Task<ICollection<ListagemUsuarioDTO>> GetUsuarios(int? Pag, int UsuariosPorPag);
        Task NovoUsuario(UsuarioPostParam newUsuario);
        Task EditaUsuario(UsuarioPutParam newUsuario, Guid UsuarioComumId);
        Task DeleteUsuario(Guid UsuarioComumId);
    }
}

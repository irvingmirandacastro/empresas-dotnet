﻿using empresas_dotnet.Domain.Requests.Login;
using empresas_dotnet.Domain.Response.Login;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Interfaces.Services
{
    public interface ILoginService
    {
        Task<LoginResponse> LoginAdmin(LoginParam login, string tokenKey);
        Task<LoginResponse> LoginComum(LoginParam login, string tokenKey);
    }
}

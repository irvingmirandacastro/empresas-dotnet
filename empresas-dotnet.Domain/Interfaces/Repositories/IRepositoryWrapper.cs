﻿namespace empresas_dotnet.Domain.Interfaces.Repositories
{
    public interface IRepositoryWrapper
    {
        IFilmeRepository Filmes { get; }
        ICategoriaFilmeRepository CategoriasFilmes { get; }
        IAtorFilmeRepository AtoresFilmes { get; }
        IVotoRepository Votos { get; }
        IUsuarioAdminRepository UsuariosAdmins { get; }
        IUsuarioComumRepository UsuariosComuns { get; }
    }
}

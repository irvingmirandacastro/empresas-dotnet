﻿using empresas_dotnet.Domain.Entities;

namespace empresas_dotnet.Domain.Interfaces.Repositories
{
    public interface IVotoRepository : IBaseRepository<Voto>
    {
    }
}

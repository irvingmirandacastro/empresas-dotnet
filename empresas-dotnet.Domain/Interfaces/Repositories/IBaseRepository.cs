﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace empresas_dotnet.Domain.Interfaces.Repositories
{
    public interface IBaseRepository<T>
    {
        Task Create(T entity);
        Task CreateMany(ICollection<T> entities);
        Task<ICollection<T>> Find(Expression<Func<T, bool>> expression, int? skip = null, int? take = null, ICollection<string> includes = null);
        Task<T> FindSingle(Expression<Func<T, bool>> expression, ICollection<string> includes = null);
        Task Save();
    }
}

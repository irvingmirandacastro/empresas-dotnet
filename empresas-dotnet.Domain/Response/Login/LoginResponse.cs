﻿using System;

namespace empresas_dotnet.Domain.Response.Login
{
    public class LoginResponse
    {
        public string Token { get; set; }
        public DateTime? TempoSessao { get; set; }
    }
}

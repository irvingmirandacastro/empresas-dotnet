﻿using AutoMapper;
using System.Linq;

namespace empresas_dotnet.Domain.Response.Filme
{
    public class FilmeMapperProfile : Profile
    {
        public FilmeMapperProfile()
        {
            CreateMap<Entities.Filme, DetalhesFilmeDTO>()
                .ForMember(c => c.MediaVotos, d => d.MapFrom(f => f.Votos.Count > 0 ? (from a in f.Votos select a.Nota).Average() : 0))
                .ForMember(c => c.Categorias, d => d.MapFrom(f => (from a in f.Categorias select a.Categoria.Nome)))
                .ForMember(c => c.Atores, d => d.MapFrom(f => (from a in f.Atores select a.Ator.Nome)));

            CreateMap<Entities.Filme, ListagemFilmesDTO>()
                .ForMember(c => c.VotoCount, d => d.MapFrom(f => f.Votos.Count));
        }
    }
}

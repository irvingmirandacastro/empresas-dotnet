﻿using System;

namespace empresas_dotnet.Domain.Response.Filme
{
    public class ListagemFilmesDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string PosterURL { get; set; }
        public int VotoCount { get; set; }
    }
}

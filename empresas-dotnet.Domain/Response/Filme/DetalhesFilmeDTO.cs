﻿using System;
using System.Collections.Generic;

namespace empresas_dotnet.Domain.Response.Filme
{
    public class DetalhesFilmeDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Diretor { get; set; }
        public string PosterURL { get; set; }
        public string Descricao { get; set; }
        public double MediaVotos { get; set; }
        public ICollection<string> Atores { get; set; }
        public ICollection<string> Categorias { get; set; }
    }
}

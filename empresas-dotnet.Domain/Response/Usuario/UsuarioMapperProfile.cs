﻿using AutoMapper;

namespace empresas_dotnet.Domain.Response.Usuario
{
    public class UsuarioMapperProfile : Profile
    {
        public UsuarioMapperProfile()
        {
            CreateMap<Entities.UsuarioAdmin, ListagemUsuarioDTO>();
            CreateMap<Entities.UsuarioComum, ListagemUsuarioDTO>();
        }
    }
}

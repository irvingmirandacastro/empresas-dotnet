﻿namespace empresas_dotnet.Domain.Response.Usuario
{
    public class ListagemUsuarioDTO
    {
        public string Nome { get; set; }
        public string Email { get; set; }
    }
}

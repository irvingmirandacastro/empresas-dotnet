﻿using System;
using System.Collections.Generic;

namespace empresas_dotnet.Domain.Requests.Filme
{
    public class FilmePostParam
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Diretor { get; set; }
        public string PosterURL { get; set; }
        public ICollection<Guid> AtoresIds { get; set; }
        public ICollection<Guid> CateogoriasIds { get; set; }
    }
}

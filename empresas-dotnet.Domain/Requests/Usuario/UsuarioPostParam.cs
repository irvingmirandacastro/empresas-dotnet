﻿namespace empresas_dotnet.Domain.Requests.Usuario
{
    public class UsuarioPostParam
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}

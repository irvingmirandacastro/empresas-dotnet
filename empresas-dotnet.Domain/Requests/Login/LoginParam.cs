﻿namespace empresas_dotnet.Domain.Requests.Login
{
    public class LoginParam
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}

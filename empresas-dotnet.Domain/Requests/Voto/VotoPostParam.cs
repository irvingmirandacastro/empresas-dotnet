﻿using System;

namespace empresas_dotnet.Domain.Requests.Voto
{
    public class VotoPostParam
    {
        public Guid FilmeId { get; set; }
        public int Nota { get; set; } // 0 - 4
    }
}

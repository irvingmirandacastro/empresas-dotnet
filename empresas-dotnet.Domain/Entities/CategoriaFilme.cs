﻿using System;

namespace empresas_dotnet.Domain.Entities
{
    public class CategoriaFilme : BaseEntity
    {
        public CategoriaFilme(Guid CategoriaId, Guid FilmeId)
        {
            this.CategoriaId = CategoriaId;
            this.FilmeId = FilmeId;
        }

        public Guid CategoriaId { get; set; }
        public Categoria Categoria { get; set; }
        public Guid FilmeId { get; set; }
        public Filme Filme { get; set; }
    }
}

﻿using System;

namespace empresas_dotnet.Domain.Entities
{
    public abstract class Usuario : BaseEntity
    {
        public Usuario(string Nome, string Email, string Senha) : base()
        {
            this.Nome = Nome;
            this.Email = Email;
            this.Senha = Senha;
        }
        public void Update(string Nome, string Email, string Senha)
        {
            this.Nome = Nome ?? this.Nome;
            this.Email = Email ?? this.Email;
            this.Senha = Senha ?? this.Senha;
            this.UpdatedAt = DateTime.Now;
        }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Senha { get; private set; }
    }
}

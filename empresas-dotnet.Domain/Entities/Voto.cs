﻿using System;

namespace empresas_dotnet.Domain.Entities
{
    public class Voto : BaseEntity
    {
        public Voto(Guid UsuarioComumId, Guid FilmeId, int Nota) : base()
        {
            this.UsuarioComumId = UsuarioComumId;
            this.FilmeId = FilmeId;
            this.Nota = Nota;
        }

        public Guid UsuarioComumId { get; private set; }
        public UsuarioComum UsuarioComum { get; set; }
        public Guid FilmeId { get; private set; }
        public Filme Filme { get; set; }
        public int Nota { get; private set; } // 0 - 4
    }
}

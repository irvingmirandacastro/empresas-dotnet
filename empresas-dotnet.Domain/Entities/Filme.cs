﻿using System.Collections.Generic;

namespace empresas_dotnet.Domain.Entities
{
    public class Filme : BaseEntity
    {
        public Filme(string Nome, string Diretor, string Descricao, string PosterURL) : base()
        {
            this.Nome = Nome;
            this.Diretor = Diretor;
            this.Descricao = Descricao;
            this.PosterURL = PosterURL;
        }
        public string Nome { get; private set; }
        public string Diretor { get; private set; }
        public string Descricao { get; private set; }
        public string PosterURL { get; private set; }
        public ICollection<Voto> Votos { get; set; }
        public ICollection<CategoriaFilme> Categorias { get; set; }
        public ICollection<AtorFilme> Atores { get; set; }
    }
}

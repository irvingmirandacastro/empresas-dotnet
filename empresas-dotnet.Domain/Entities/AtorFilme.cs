﻿using System;

namespace empresas_dotnet.Domain.Entities
{
    public class AtorFilme : BaseEntity
    {
        public AtorFilme(Guid AtorId, Guid FilmeId)
        {
            this.AtorId = AtorId;
            this.FilmeId = FilmeId;
        }

        public Guid AtorId { get; set; }
        public Ator Ator { get; set; }
        public Guid FilmeId { get; set; }
        public Filme Filme { get; set; }
    }
}

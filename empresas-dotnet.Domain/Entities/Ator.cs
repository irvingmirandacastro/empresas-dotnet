﻿using System.Collections.Generic;

namespace empresas_dotnet.Domain.Entities
{
    public class Ator : BaseEntity
    {
        public Ator(string Nome) : base()
        {
            this.Nome = Nome;
        }
        public string Nome { get; private set; }
        public ICollection<AtorFilme> Filmes { get; set; }
    }
}

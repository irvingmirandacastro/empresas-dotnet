﻿using System.Collections.Generic;

namespace empresas_dotnet.Domain.Entities
{
    public class Categoria : BaseEntity
    {
        public Categoria(string Nome) : base()
        {
            this.Nome = Nome;
        }
        public string Nome { get; private set; }
        public ICollection<CategoriaFilme> Filmes { get; set; }
    }
}

﻿using System;

namespace empresas_dotnet.Domain.Entities
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
            Ativo = true;
            CreatedAt = DateTime.Now;
            UpdatedAt = null;
        }
        public void TrocaEstado(bool Ativo)
        {
            this.Ativo = Ativo;
        }
        public Guid Id { get; set; }
        public bool Ativo { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime? UpdatedAt { get; protected set; }
    }
}

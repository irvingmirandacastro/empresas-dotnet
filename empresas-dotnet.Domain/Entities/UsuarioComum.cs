﻿using System.Collections.Generic;

namespace empresas_dotnet.Domain.Entities
{
    public class UsuarioComum : Usuario
    {
        public UsuarioComum(string Nome, string Email, string Senha) : base(Nome, Email, Senha) { }
        public ICollection<Voto> Votos { get; set; }
    }
}

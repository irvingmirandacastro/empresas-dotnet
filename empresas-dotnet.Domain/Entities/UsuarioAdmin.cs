﻿namespace empresas_dotnet.Domain.Entities
{
    public class UsuarioAdmin : Usuario
    {
        public UsuarioAdmin(string Nome, string Email, string Senha) : base(Nome, Email, Senha) { }
    }
}

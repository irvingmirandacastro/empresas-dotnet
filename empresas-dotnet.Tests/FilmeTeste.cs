using AutoMapper;
using empresas_dotnet.Domain.Entities;
using empresas_dotnet.Domain.Interfaces.Repositories;
using empresas_dotnet.Domain.Response.Filme;
using empresas_dotnet.Service.Services;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace empresas_dotnet.Tests
{
    public class FilmeTeste
    {
        private readonly FilmeService Service;
        private async Task<ICollection<Filme>> testData()
        {
            return new List<Filme>() {
                new Filme("Teste", "Teste", "Teste", "Teste"),
                new Filme("Teste2", "Teste2", "Teste2", "Teste2"),
                new Filme("Teste3", "Teste3", "Teste3", "Teste3")
            };
        }
        public FilmeTeste()
        {
            var mockRepWrapper = new Mock<IRepositoryWrapper>();
            var mockMapper = new Mock<IMapper>();
            mockRepWrapper.Setup(repo =>
                repo.Filmes.FindSingle(It.IsAny<Expression<Func<Filme, bool>>>(), It.IsAny<ICollection<string>>())
            ).Returns(Task.FromResult(new Filme("Teste", "Teste", "Teste", "Teste")));
            mockRepWrapper.Setup(repo =>
                repo.Filmes.Find(It.IsAny<Expression<Func<Filme, bool>>>(), It.IsAny<int?>(), It.IsAny<int?>(), It.IsAny<ICollection<string>>())
            ).Returns(testData());
            mockMapper.Setup(map =>
                map.Map<ICollection<ListagemFilmesDTO>>(It.IsAny<ICollection<Filme>>())
            ).Returns(new List<ListagemFilmesDTO>());
            mockMapper.Setup(map =>
                map.Map<DetalhesFilmeDTO>(It.IsAny<Filme>())
            ).Returns(new DetalhesFilmeDTO());

            Service = new FilmeService(mockRepWrapper.Object, mockMapper.Object);
        }
        [Fact]
        public async void GetFilme()
        {
            var result = await Service.GetFilme(Guid.NewGuid());
            result.Should().BeOfType(typeof(DetalhesFilmeDTO));
        }
        [Fact]
        public async void GetFilmes()
        {
            var result = await Service.GetFilmes(null, null, null, null, null, 0);
            result.Should().BeOfType(typeof(List<ListagemFilmesDTO>));
        }
    }
}
